
+-+-+-+-+-+-+-+-+
|i|R|a|M|u|T|e|Q| - Fri Jan 21 20:01:42 2022
+-+-+-+-+-+-+-+-+


Number of texts: 2967
Number of text segments: 2972
Number of forms: 2881
Number of occurrences: 48837
Number of lemmas: 2126
Number of active forms: 1795
Number of supplementary forms: 331
Number of active forms with a frequency >= 3: 880
Mean of forms by segment: 16.432369
Number of clusters: 8
2895 segments classified on 2972 (97.41%)

###########################
time : 0h 0m 49s
###########################
