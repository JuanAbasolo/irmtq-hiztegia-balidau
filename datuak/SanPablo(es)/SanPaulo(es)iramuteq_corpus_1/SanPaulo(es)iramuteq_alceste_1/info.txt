
+-+-+-+-+-+-+-+-+
|i|R|a|M|u|T|e|Q| - Fri Jan 21 19:33:00 2022
+-+-+-+-+-+-+-+-+


Number of texts: 2663
Number of text segments: 2669
Number of forms: 4759
Number of occurrences: 45740
Number of lemmas: 2492
Number of active forms: 2368
Number of supplementary forms: 121
Number of active forms with a frequency >= 3: 996
Mean of forms by segment: 17.137505
Number of clusters: 9
2629 segments classified on 2669 (98.50%)

###########################
time : 0h 0m 42s
###########################
