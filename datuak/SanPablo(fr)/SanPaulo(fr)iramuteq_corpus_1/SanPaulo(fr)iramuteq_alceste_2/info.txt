
+-+-+-+-+-+-+-+-+
|i|R|a|M|u|T|e|Q| - Sat Jan 22 23:52:13 2022
+-+-+-+-+-+-+-+-+


Number of texts: 2336
Number of text segments: 2438
Number of forms: 4437
Number of occurrences: 46342
Number of lemmas: 2699
Number of active forms: 2427
Number of supplementary forms: 272
Number of active forms with a frequency >= 3: 1009
Mean of forms by segment: 19.008203
Number of clusters: 7
2278 segments classified on 2438 (93.44%)

###########################
time : 0h 0m 57s
###########################
