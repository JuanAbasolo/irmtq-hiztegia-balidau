
+-+-+-+-+-+-+-+-+
|i|R|a|M|u|T|e|Q| - Wed Jan  5 01:03:46 2022
+-+-+-+-+-+-+-+-+


Number of texts: 2669
Number of text segments: 2669
Number of forms: 6544
Number of occurrences: 32143
Number of lemmas: 3847
Number of active forms: 3560
Number of supplementary forms: 287
Number of active forms with a frequency >= 3: 1112
Mean of forms by segment: 12.043087
Number of clusters: 5
2642 segments classified on 2669 (98.99%)

###########################
time : 0h 0m 28s
###########################
