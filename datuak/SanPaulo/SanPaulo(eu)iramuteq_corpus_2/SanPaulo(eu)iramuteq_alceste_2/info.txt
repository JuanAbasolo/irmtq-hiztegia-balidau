
+-+-+-+-+-+-+-+-+
|i|R|a|M|u|T|e|Q| - Sun Jan 16 18:25:45 2022
+-+-+-+-+-+-+-+-+


Number of texts: 2669
Number of text segments: 2669
Number of forms: 6544
Number of occurrences: 32143
Number of lemmas: 3759
Number of active forms: 3453
Number of supplementary forms: 178
Number of active forms with a frequency >= 3: 1124
Mean of forms by segment: 12.043087
Number of clusters: 11
2515 segments classified on 2669 (94.23%)

###########################
time : 0h 1m 3s
###########################
