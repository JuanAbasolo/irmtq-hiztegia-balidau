
+-+-+-+-+-+-+-+-+
|i|R|a|M|u|T|e|Q| - Tue Dec 21 11:39:45 2021
+-+-+-+-+-+-+-+-+


Number of texts: 4733
Number of text segments: 4745
Number of forms: 6160
Number of occurrences: 53547
Number of lemmas: 3556
Number of active forms: 3270
Number of supplementary forms: 286
Number of active forms with a frequency >= 3: 1436
Mean of forms by segment: 11.284932
Number of clusters: 12
4732 segments classified on 4745 (99.73%)

###########################
time : 0h 0m 36s
###########################
