
+-+-+-+-+-+-+-+-+
|i|R|a|M|u|T|e|Q| - Sun Apr 24 23:49:04 2022
+-+-+-+-+-+-+-+-+


Number of texts: 1155
Number of text segments: 1158
Number of forms: 1943
Number of occurrences: 16371
Number of lemmas: 1203
Number of active forms: 1117
Number of supplementary forms: 84
Number of active forms with a frequency >= 3: 421
Mean of forms by segment: 14.137306
Number of clusters: 4
1104 segments classified on 1158 (95.34%)

###########################
time : 0h 0m 26s
###########################
