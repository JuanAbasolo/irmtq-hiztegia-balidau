
+-+-+-+-+-+-+-+-+
|i|R|a|M|u|T|e|Q| - Sun Apr 24 23:39:54 2022
+-+-+-+-+-+-+-+-+


Number of texts: 1155
Number of text segments: 1158
Number of forms: 1943
Number of occurrences: 16371
Number of lemmas: 1203
Number of active forms: 1117
Number of supplementary forms: 84
Number of active forms with a frequency >= 3: 421
Mean of forms by segment: 14.137306
Number of clusters: 8
1141 segments classified on 1158 (98.53%)

###########################
time : 0h 0m 29s
###########################
